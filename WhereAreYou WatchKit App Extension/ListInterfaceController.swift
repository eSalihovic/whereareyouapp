//
//  ListInterfaceController.swift
//  WhereAreYou WatchKit Extension
//
//  Created by Nikola Jovic on 30/11/2017.
//  Copyright © 2017 qsdbih. All rights reserved.
//

import WatchKit
import CoreData
import Foundation
import WatchConnectivity

class ListInterfaceController: WKInterfaceController, WCSessionDelegate {
    
    static let MaximumNumberOfItems = 10
    static let MetersPerMile = 1609.344
    static let MetersPerFoot = 0.3048
    
    @IBOutlet var noMessagesLabel: WKInterfaceLabel!
    @IBOutlet var inviteLabel: WKInterfaceLabel!
    @IBOutlet var table: WKInterfaceTable!
    
    var messages: [Message]?
    //var messages: [[String: Any]]?
    var session: WCSession!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        //configureTable()
    }
    
    override func willActivate() {
        super.willActivate()
        if WCSession.isSupported() {
            session = WCSession.default
            session.delegate = self
            session.activate()
        }
        configureTable()
    }
    
    private func configureTable() {
        self.messages = fetchMesssages()
        
        guard let messages = self.messages, !messages.isEmpty else {
            // no messages, so show the 'no message' message
            print("no messages")
            table.setHidden(true)
            noMessagesLabel.setHidden(false)
            inviteLabel.setHidden(false)
            return
        }
        
        table.setHidden(false)
        noMessagesLabel.setHidden(true)
        inviteLabel.setHidden(true)
        table.setNumberOfRows(messages.count, withRowType: "MessageRow")
        
        var coordinates: [[String:Double]] = []
        for (index, message) in messages.enumerated() {
            let controller = table.rowController(at: index) as! MessageTableRow
            controller.nameLabel.setText(message.name)
            controller.distLabel.setText("")
            controller.timeLabel.setText(message.timeSpan())
            coordinates.append(["latitude": message.latitude, "longitude": message.longitude])
        }
    }
    
    override func contextForSegue(withIdentifier segueIdentifier: String, in table: WKInterfaceTable, rowIndex: Int) -> Any? {
        guard let messages = self.messages else { return nil }
        return messages[rowIndex]
    }
    
    override func handleAction(withIdentifier identifier: String?, forRemoteNotification remoteNotification: [AnyHashable : Any]) {
        
    }
    
    func session(_ session: WCSession, didReceiveUserInfo userInfo: [String : Any] = [:]) {
        guard let msgs = userInfo["message"] as? [[String: Any]] else { return }
        for m in msgs {
            guard let token = m["token"] as? String else { return }
            var message = Message.fetch(byToken: token)
            if message == nil {
                message = CoreDataManager.shared.createMessage()
                message?.token = token
            }
            else {
                message?.latitude = 0.0
                message?.longitude = 0.0
            }
            if let name = m["name"] as? String {
                message?.name = name
            }
            
            if let answerDate = m["answerDate"] as? Date {
                message?.answerDate = answerDate
            }
            
            if let latitude = m["latitude"] as? Double {
                message?.latitude = latitude
            }
            
            if let longitude = m["longitude"] as? Double {
                message?.longitude = longitude
            }
            
            CoreDataManager.shared.saveContext()
        }
        
        findDeletedMessage(msgs)
        configureTable()
    }
    
    private func findDeletedMessage(_ deletedMessages: [[String: Any]]) {
        var tokens = [String]()
        guard let msg = fetchMesssages() else { return }
        for m in msg {
            guard let t = m.token else { continue }
            if !deletedMessages.contains(where: { (deleted) -> Bool in
                deleted["token"] as? String == t
            }) {
                tokens.append(t)
            }
        }
        
        guard !tokens.isEmpty else { return }
        for t in tokens {
            guard let message = Message.fetch(byToken: t) else { continue }
            CoreDataManager.shared.context.delete(message)
            try? CoreDataManager.shared.context.save()
        }
    }
    
    private func fetchMesssages() -> [Message]? {
        var messages: [Message]?
        let ctx = CoreDataManager.shared.context
        let fr: NSFetchRequest<Message> = Message.fetchRequest()
        fr.fetchBatchSize = ListInterfaceController.MaximumNumberOfItems
        
        do {
            messages = try ctx.fetch(fr)
        } catch {
            fatalError("Failed to fetch message: \(error)")
        }
        return messages
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
}
