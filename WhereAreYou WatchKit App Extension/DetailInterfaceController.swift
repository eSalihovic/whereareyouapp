//
//  DetailInterfaceController.swift
//  WhereAreYou WatchKit Extension
//
//  Created by Nikola Jovic on 30/11/2017.
//  Copyright © 2017 qsdbih. All rights reserved.
//

import WatchKit
import MapKit

class DetailInterfaceController: WKInterfaceController {
    
    static var COORDINATE_SPAN: MKCoordinateSpan {
        let span = 0.01
        return MKCoordinateSpanMake(span, span)
    }
    
    @IBOutlet var nameLabel: WKInterfaceLabel!
    @IBOutlet var map: WKInterfaceMap!
    
    var message: Message!
    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        guard let message = context as? Message else {
            print("Can not cast context param as Message type")
            return
        }
        self.message = message
        nameLabel.setText(message.name)
        map.setRegion(MKCoordinateRegionMake(message.coordinate, DetailInterfaceController.COORDINATE_SPAN))
        map.addAnnotation(message.coordinate, with: .red)
    }
    
}
