//
//  NotificationController.swift
//  WhereAreYou WatchKit App Extension
//
//  Created by Nikola Jovic on 26/11/2017.
//  Copyright © 2017 qsdbih. All rights reserved.
//

import WatchKit
import Foundation
import UserNotifications


class NotificationController: WKUserNotificationInterfaceController {

    @IBOutlet var nameLabel: WKInterfaceLabel!
    @IBOutlet var map: WKInterfaceMap!
    
    
    override init() {
        // Initialize variables here.
        super.init()
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    /*
    override func didReceive(_ notification: UNNotification, withCompletion completionHandler: @escaping (WKUserNotificationInterfaceType) -> Swift.Void) {
        // This method is called when a notification needs to be presented.
        // Implement it if you use a dynamic notification interface.
        // Populate your dynamic notification interface as quickly as possible.
        //
        // After populating your dynamic notification interface call the completion block.
        completionHandler(.custom)
    }
    */

//    override func didReceive(_ localNotification: UILocalNotification, withCompletion completionHandler: @escaping (WKUserNotificationInterfaceType) -> Void) {
//
//    }
    
    
    override func didReceiveRemoteNotification(_ remoteNotification: [AnyHashable : Any], withCompletion completionHandler: @escaping (WKUserNotificationInterfaceType) -> Void) {
        
        print(remoteNotification)
        
        guard let aps = remoteNotification["aps"] as? NSDictionary, let alert = aps["alert"] as? String else {
            print("Could not cast alert message for remote notification.")
            return
        }
        nameLabel.setText(alert.components(separatedBy: " ").first)
        
        guard let wArray = remoteNotification["w"] as? [Double] else {
            print("Could not cast coordinates array for remote notification.")
            return
        }
        let coordinate = CLLocationCoordinate2D(latitude: wArray[3], longitude: wArray[2])
        map.setRegion(MKCoordinateRegion(center: coordinate, span: DetailInterfaceController.COORDINATE_SPAN))
        map.addAnnotation(coordinate, with: .red)
        
        completionHandler(.custom)
    }
}
