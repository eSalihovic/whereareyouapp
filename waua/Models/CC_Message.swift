//
//  CC_Message.swift
//  waua
//
//  Created by Nikola Jovic on 04/01/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import Foundation
import CoreData

@objc(CC_Message)
public class CC_Message: NSManagedObject {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<CC_Message> {
        return NSFetchRequest<CC_Message>(entityName: "Message")
    }
    
    @NSManaged public var accuracy: Float
    @NSManaged public var answeDate: Date?
    @NSManaged public var latitude: Float
    @NSManaged public var longitude: Float
    @NSManaged public var name: String?
    @NSManaged public var number: String?
    @NSManaged public var requestDate: Date?
    @NSManaged public var status: Int
    @NSManaged public var token: String?
}
