//
//  Message.swift
//  waua
//
//  Created by Edin on 11/23/17.
//  Copyright © 2017 qsdbih. All rights reserved.
//

import Foundation
import CoreData
import MapKit

// time constants
let MINUTE: Double = 60.0
let HOUR: Double = MINUTE * 60.0
let DAY: Double = HOUR * 24.0
let WEEK: Double = DAY * 7.0
let MONTH: Double = WEEK * 4.345
let YEAR: Double = DAY * 365
let JUST_NOW_DURATION: Double = 8.0


// message state values
enum MessageState: String {
    
    case messageSent = "Sent"
    case messageAccepted = "Accepted"
    case messageDenied = "Denied"
    case messageTimeout = "Timeout"
    case messageDeleted = "Deleted" // should never be visible to the app
    
    static func fromNubmer(statusNumber: Int) -> MessageState? {
        switch statusNumber {
        case 0: return MessageState.messageSent
        case 1: return MessageState.messageAccepted
        case 2: return MessageState.messageDenied
        case 3: return MessageState.messageTimeout
        case 4: return MessageState.messageDeleted
        default: return nil
        }
    }
}

@objc(Message)
public class Message: NSManagedObject {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Message> {
        return NSFetchRequest<Message>(entityName: "Message")
    }
    
    @NSManaged public var accuracy: Double
    @NSManaged public var answerDate: Date?
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var name: String?
    @NSManaged public var number: String?
    @NSManaged public var requestDate: Date?
    @NSManaged public var status: String?
    @NSManaged public var token: String?
}

extension Message {
    
    // get the managed object context
    
    class var context: NSManagedObjectContext {
        return CoreDataManager.shared.context
    }
    
    class func fetch(byToken token: String) -> Message? {
        var message: Message?
        let context = self.context
        let fetchRequest: NSFetchRequest<Message> = Message.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "token == %@", token)
        
        do {
            message = try context.fetch(fetchRequest).first
        } catch {
            fatalError("Failed to fetch message: \(error)")
        }
        
        return message
    }
    
    class func createDbCtxMsg() -> Message {
        return NSEntityDescription.insertNewObject(forEntityName: "Message", into: context) as? Message ?? Message()
    }
    
    // get the location
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2DMake(latitude, longitude)
    }
    
    // for the MKCircleView
    func radius() -> CLLocationDistance {
        return accuracy
    }
    
    // get the bounding rect
    var boundingMapRect: MKMapRect {
        let upperLeft: MKMapPoint = MKMapPointForCoordinate(coordinate)
        let accuracy: Double = self.accuracy
        let bounds: MKMapRect = MKMapRectMake(upperLeft.x - accuracy * 2, upperLeft.y - accuracy * 2, accuracy * 4, accuracy * 4)
        return bounds
    }
    
    // return boolean of message has a location
    func hasLocation() -> Bool {
        return longitude != 0.0 || latitude != 0.0
    }
    
    // turn a string from the server into a date object
    func dateFrom(string: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.date(from: string)
    }
    
    // get the tip title text in the map
    var title: String? {
        return self.name
    }
    
    // get the subtitle text in the map
    var subtitle: String? {
        guard let answerDate = answerDate else { return nil }
        let interval = Date().timeIntervalSince(answerDate)
        if interval >= DAY {
            return timeSpan()
        }
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "HH:mm"
        return String(format: NSLocalizedString("TimeWasHere", comment: ""), timeFormatter.string(from: answerDate))
    }
    
    func TIMESPAN(interval: TimeInterval, UPPER: Double, N1: String, LOWER: Double, N2: String) -> String {
        let v1 = Int(interval / UPPER)
        let v2 = Int((Int(interval) - v1 * Int(UPPER)) / Int(LOWER))
        
        let key = String(format: "Time%@%@", (v1 > 1 ? N1 + "s" : N1), (v2 == 0 ? "" : (v2 > 1 ? N2 + "s" : N2)))
        return String(format: NSLocalizedString(key, comment: ""), v1, v2)
    }
    
    // get time since the answerdate as string
    func timeSpan() -> String? {
        guard let answerDate = answerDate else { return nil }
        let interval = Date().timeIntervalSince(answerDate)
        if interval >= YEAR {
            return TIMESPAN(interval: interval, UPPER: YEAR, N1: "Year", LOWER: MONTH, N2: "Month")
        }
        else if interval >= MONTH {
            return TIMESPAN(interval: interval, UPPER: MONTH, N1: "Month", LOWER: WEEK, N2: "Week")
        }
        else if interval >= WEEK {
            return TIMESPAN(interval: interval, UPPER: WEEK, N1: "Week", LOWER: DAY, N2: "Day")
        }
        else if interval >= DAY {
            return TIMESPAN(interval: interval, UPPER: DAY, N1: "Day", LOWER: HOUR, N2: "Hour")
        }
        else if interval >= HOUR {
            return TIMESPAN(interval: interval, UPPER: HOUR, N1: "Hour", LOWER: MINUTE, N2: "Minute")
        }
        else if interval >= MINUTE {
            return TIMESPAN(interval: interval, UPPER: MINUTE, N1: "Minute", LOWER: 1, N2: "Second")
        }
        else if interval > JUST_NOW_DURATION {
            let seconds = Int(interval)
            // seconds
            return String(format: NSLocalizedString("TimeSeconds", comment: ""), seconds)
        }
        
        return NSLocalizedString("TimeJustNow", comment: "")
    }
}
