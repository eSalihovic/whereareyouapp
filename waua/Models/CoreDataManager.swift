//
//  CoreDataManager.swift
//  waua
//
//  Created by Edin on 10/11/17.
//  Copyright © 2017 qsdbih. All rights reserved.
//

import Foundation
import CoreData

#if !EXTENSION
    //import Flurry
#endif

let kAppGroupIdentifier = "group.com.coandcouk.waua"

class CoreDataManager: NSObject {
    
    static let shared = CoreDataManager()
    
    var context: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    // save message into the database
    func createMessage() -> Message {
        return NSEntityDescription.insertNewObject(forEntityName: "Message", into: context) as? Message ?? Message()
    }
    
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Model")
        let storeUrl =  FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: kAppGroupIdentifier)!.appendingPathComponent("Final_DB.sqlite")
        let description = NSPersistentStoreDescription()
        description.shouldInferMappingModelAutomatically = true
        description.shouldMigrateStoreAutomatically = true
        description.url = storeUrl
        container.persistentStoreDescriptions = [NSPersistentStoreDescription(url: storeUrl)]
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                print("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                print("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    class func migrateCoreDataToNewSwiftAppIfNeeded() {
        let userDefaults = UserDefaults(suiteName: kAppGroupIdentifier)
        let needsMigrate = !(userDefaults?.bool(forKey: "core_data_did_migrate_to_new_swift_app") ?? false)
        guard needsMigrate else { return }
        
        print("----- Core data needs to migrate into new swift app! -----")
        
        // Type 1: OBJ-C legacy app
        let container1 = NSPersistentContainer(name: "Model_Legacy")
        let storeUrl1 =  FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: kAppGroupIdentifier)!.appendingPathComponent("Model.sqlite")
        let description1 = NSPersistentStoreDescription()
        description1.shouldInferMappingModelAutomatically = true
        description1.shouldMigrateStoreAutomatically = true
        description1.url = storeUrl1
        container1.persistentStoreDescriptions = [NSPersistentStoreDescription(url: storeUrl1)]
        container1.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                print("An error occured while opening OBJ-C legacy app persistent store!")
                print(error)
            } else {
                print("Migrating data from OBJ-C legacy app")
                var messages: [CC_Message]?
                let context = container1.viewContext
                let fetchRequest: NSFetchRequest<CC_Message> = CC_Message.fetchRequest()
                let sortDescriptor = NSSortDescriptor(key: "requestDate", ascending: false)
                fetchRequest.sortDescriptors = [sortDescriptor]
                do {
                    messages = try context.fetch(fetchRequest)
                } catch {
                    print("Failed to fetch messages: \(error)")
                    return
                }

                print("Migrating \(messages?.count ?? 0) messages into new persisten store Final_DB")
                for message in messages ?? [] {
                    let newMessage = Message.createDbCtxMsg()
                    newMessage.accuracy = Double(message.accuracy)
                    newMessage.answerDate = message.answeDate
                    newMessage.latitude = Double(message.latitude)
                    newMessage.longitude = Double(message.longitude)
                    newMessage.name = message.name
                    newMessage.number = message.number
                    newMessage.requestDate = message.requestDate
                    newMessage.status = MessageState.fromNubmer(statusNumber: message.status)?.rawValue ?? MessageState.messageAccepted.rawValue
                    newMessage.token = message.token
                }
                CoreDataManager.shared.saveContext()
                print("Migrating data from OBJ-C legacy app did finish.")
            }
        })
        
        
        // Type 2: If user have clean install of first release of swift app
        let container2 = NSPersistentContainer(name: "Model")
        let storeUrl2 =  FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: kAppGroupIdentifier)!.appendingPathComponent("Model.sqlite")
        let description2 = NSPersistentStoreDescription()
        description2.shouldInferMappingModelAutomatically = true
        description2.shouldMigrateStoreAutomatically = true
        description2.url = storeUrl2
        container2.persistentStoreDescriptions = [NSPersistentStoreDescription(url: storeUrl2)]
        container2.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                print("An error occured while opening fisrt swift app persistent store!")
                print(error)
            } else {
                print("Migrating data from fisrt swift app")
                var messages: [Message]?
                let context = container2.viewContext
                let fetchRequest: NSFetchRequest<Message> = Message.fetchRequest()
                let sortDescriptor = NSSortDescriptor(key: "requestDate", ascending: false)
                fetchRequest.sortDescriptors = [sortDescriptor]
                do {
                    messages = try context.fetch(fetchRequest)
                } catch {
                    print("Failed to fetch messages: \(error)")
                    return
                }
                
                print("Migrating \(messages?.count ?? 0) messages into new persisten store Final_DB")
                for message in messages ?? [] {
                    let newMessage = Message.createDbCtxMsg()
                    newMessage.accuracy = message.accuracy
                    newMessage.answerDate = message.answerDate
                    newMessage.latitude = message.latitude
                    newMessage.longitude = message.longitude
                    newMessage.name = message.name
                    newMessage.number = message.number
                    newMessage.requestDate = message.requestDate
                    newMessage.status = message.status
                    newMessage.token = message.token
                }
                CoreDataManager.shared.saveContext()
            }
        })
        
        userDefaults?.set(true, forKey: "core_data_did_migrate_to_new_swift_app")
        userDefaults?.synchronize()
    }
}
