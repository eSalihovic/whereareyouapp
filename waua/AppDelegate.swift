//
//  AppDelegate.swift
//  waua
//
//  Created by Edin on 9/27/17.
//  Copyright © 2017 qsdbih. All rights reserved.
//

import UIKit
import Contacts
import UserNotifications
import StoreKit
import Fabric
import Crashlytics
import CoreData
import WatchConnectivity
import FacebookCore

let kMaximumNumberOfItems = 10

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, WCSessionDelegate {
    
    static let shared = AppDelegate()
    
    var window: UIWindow?
    var store = CNContactStore()
    var session: WCSession!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        Fabric.with([Crashlytics.self])
        
        CoreDataManager.migrateCoreDataToNewSwiftAppIfNeeded()
        
        registerForPushNotifications()
        
        AppStoreService.shared.startProductsRequest()
        
        if WCSession.isSupported() {
            session = WCSession.default
            session.delegate = self
            session.activate()
        }
        
        guard let notification = launchOptions?[.remoteNotification] as? [AnyHashable: Any] else { return true }
        guard let listData = ListData.fromNotification(userInfo: notification) else { return true}
        guard let message = updateMessage(listData) else { return true }
        open(message)
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        application.applicationIconBadgeNumber = 0
        let defaults = UserDefaults.standard
        if !defaults.bool(forKey: kHasLaunchedBefore) {
            defaults.set(true, forKey: kHasLaunchedBefore)
            defaults.synchronize()
            // first run
            AppEventsLogger.activate(application)
        }
        else {
            // subsequent run
            ApiService.shared.synchronize()
            
            if (WCSession.isSupported() && session.isPaired && session.isWatchAppInstalled){
                var messages: [Message]?
                let context = CoreDataManager.shared.context
                let fetchRequest: NSFetchRequest<Message> = Message.fetchRequest()
                fetchRequest.fetchBatchSize = kMaximumNumberOfItems
                fetchRequest.predicate = NSPredicate(format: "%K == %@", "status", MessageState.messageAccepted.rawValue)
                let sortDescriptor = NSSortDescriptor(key: "requestDate", ascending: false)
                fetchRequest.sortDescriptors = [sortDescriptor]
                
                do {
                    messages = try context.fetch(fetchRequest)
                } catch {
                    fatalError("Failed to fetch message: \(error)")
                }
                
                if let msg = messages {
                    var array = [[String: Any]]()
                    var person = [String: Any]()
                    for m in msg {
                        person["token"] = m.token
                        person["name"] = m.name
                        person["answerDate"] = m.answerDate
                        person["latitude"] = m.latitude
                        person["longitude"] = m.longitude
                        array.append(person)
                    }
                    let context = ["message": array]
                    session.transferUserInfo(context)
                }
            }
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        CoreDataManager.shared.saveContext()
    }
    
    func checkAccessStatus(completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
        
        switch authorizationStatus {
        case .authorized:
            completionHandler(true)
        case .denied, .notDetermined:
            self.store.requestAccess(for: CNEntityType.contacts, completionHandler: { (access, accessError) -> Void in
                if access {
                    completionHandler(access)
                } else {
                    completionHandler(false)
                }
            })
        default:
            completionHandler(false)
        }
    }
    
    //------------------------------------------------------------------------------
    // Push notifications
    //------------------------------------------------------------------------------
    
    func registerForPushNotifications() {
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            print("Permission granted: \(granted)")
            guard granted else { return }
            self.getNotificationSettings()
        }
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    
    func setDeviceToken(_ deviceToken: String) {
        if let defaults = UserDefaults(suiteName: kAppGroupIdentifier) {
            defaults.set(deviceToken, forKey: kNotificationToken)
            defaults.synchronize()
        }
    }
    
    func deviceToken() -> String {
        if let defaults = UserDefaults(suiteName: kAppGroupIdentifier) {
            return defaults.object(forKey: kNotificationToken) as? String ?? ""
        }
        return ""
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceToken = deviceToken.hexString()
        self.setDeviceToken(deviceToken)
        ApiService.shared.synchronize()
        print("Device Token: \(deviceToken)")
    }
    
    // did receive remote notification
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("userInfo: \(userInfo)")
        guard let listData = ListData.fromNotification(userInfo: userInfo) else { return }
        //showAlert(listData: listData)
        guard let message = updateMessage(listData) else { return }
        application.applicationIconBadgeNumber += 1
        open(message)
        completionHandler(.newData)
    }
    
    private func showAlert(listData: ListData) {
        var msg = "token: \(listData.token ?? "?")\n"
        msg += "accuracy: \(listData.accuracy ?? -1)\n"
        msg += "requestDate: \(listData.request_date ?? "?")\n"
        msg += "answerDate: \(listData.answer_date ?? "?")\n"
        msg += "latitude: \(listData.latitude ?? 0)\n"
        msg += "longitude: \(listData.longitude ?? 0)\n"
        msg += "state: \(listData.status ?? "?")\n"
        let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = UIViewController()
        window.windowLevel = UIWindowLevelAlert + 1;
        window.makeKeyAndVisible()
        window.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    // received remote push notification
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        print("userInfo: \(userInfo)")
        guard let listData = ListData.fromNotification(userInfo: userInfo) else { return }
        //showAlert(listData: listData)
        guard let message = updateMessage(listData) else { return }
        
        if application.applicationState == .active {
            if updateActiveLocation(message) {
                return
            }
            guard let name = message.name else { return }
            let alertController = UIAlertController(title: name + " shared his location",
                                                    message: "Do you wish to see his location now?", preferredStyle: .alert)
            
            let action = UIAlertAction(title: "Yes", style: .default) { (alertAction) in
                self.open(message)
            }
            alertController.addAction(action)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            let window = UIWindow(frame: UIScreen.main.bounds)
            window.rootViewController = UIViewController()
            window.windowLevel = UIWindowLevelAlert + 1;
            window.makeKeyAndVisible()
            window.rootViewController?.present(alertController, animated: true, completion: nil)
        }
        else {
            open(message)
        }
    }
    
    func updateActiveLocation(_ message: Message) -> Bool {
        let tabBarCtrl = window?.rootViewController as? UITabBarController
        if tabBarCtrl?.selectedIndex != 2 {
            return false
        }
        let navCtrl = tabBarCtrl?.viewControllers?[2] as? UINavigationController
        if !(navCtrl?.topViewController is MapViewController) {
            return false
        }
        let mapView = navCtrl?.topViewController as? MapViewController
        if !(mapView?.centerLocation.token == message.token) {
            return false
        }
        mapView?.updateCenterLocation(message)
        return true
    }
    
    func updateMessage(_ notification: ListData) -> Message? {
        guard let token = notification.token else { return nil }
        let message = Message.fetch(byToken: token)
        ApiService.shared.updateDetails(notification)
        CoreDataManager.shared.saveContext()
        return message
    }
    
    // open the history "tab" and navigate to the map
    func open(_ message: Message) {
        let tabBarCtrl = window?.rootViewController as? UITabBarController
        tabBarCtrl?.selectedIndex = 2
        let navCtrl = tabBarCtrl?.viewControllers?[2] as? UINavigationController
        navCtrl?.popToRootViewController(animated: false)
        navCtrl?.viewControllers[0].performSegue(withIdentifier: "map", sender: message)
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
}
