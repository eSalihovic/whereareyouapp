//
//  ApiService.swift
//  waua
//
//  Created by Edin on 10/11/17.
//  Copyright © 2017 qsdbih. All rights reserved.
//

import Foundation
import CryptoSwift
//import Reachability

// constants
let kDeviceTokenKey = "deviceToken"
let kRequestTokenKey = "requestToken"
let kRequestTime = "requestTime"
let kHasLaunchedBefore = "HasLaunchedBefore"
let kNotificationToken = "notification-token"

/////////////////////////////////////////////////////////////////
#if DEBUG
let SERVER_URL = "http://whereareyou-app.com" // does not work -> "http://batman.coandcouk.com/waua_web"
let API_URL = SERVER_URL + "/api"
let MESSAGE_URL = SERVER_URL
let API_SALT = "kjadhsfkj@£!$ihdakj@£±$^jfhasdhkjfkj"
#else
let SERVER_URL = "http://whereareyou-app.com"
let API_URL =  SERVER_URL + "/api"
let MESSAGE_URL = SERVER_URL
let API_SALT = "kjadhsfkj@£!$ihdakj@£±$^jfhasdhkjfkj"
#endif
////////////////////////////////////////////////////////////////

struct RequestResponse: Decodable {
    let success: Bool?
    let token: String?
    let request: String?
    let url: String?
    let request_date: String?
}

struct ListResponse: Decodable {
    let success: Bool?
    let message: String?
    let token: String?
    let data: [ListData?]?
}

struct ListData: Decodable {
    
    let id: Int?
    let device_id: Int?
    let token: String?
    let url: String?
    let status: String?
    let name: String?
    let phone: String?
    let latitude: Double?
    let longitude: Double?
    let accuracy: Double?
    let request_date: String?
    let answer_date: String?
    
    static func fromNotification(userInfo: [AnyHashable: Any]) -> ListData? {
        //print(userInfo)
        guard let msgInfo = userInfo["w"] as? [Any] else { return nil }
        return ListData(id: nil,
                        device_id: nil,
                        token: msgInfo[0] as? String,
                        url: nil,
                        status: MessageState.fromNubmer(statusNumber: msgInfo[1] as? Int ?? -1)?.rawValue ?? MessageState.messageSent.rawValue,
                        name: nil,
                        phone: nil,
                        latitude: msgInfo[3] as? Double,
                        longitude: msgInfo[2] as? Double,
                        accuracy: msgInfo[4] as? Double,
                        request_date: msgInfo[5] as? String,
                        answer_date: msgInfo[6] as? String)
    }
}

class ApiService: NSObject {
    
    static let shared = ApiService()
    
    // get the notifications token
    func notificationsToken() -> String {
        if let defaults = UserDefaults(suiteName: kAppGroupIdentifier) {
            return defaults.object(forKey: kNotificationToken) as? String ?? ""
        }
        return ""
    }
    
    func getRequest(_ action: String, apiParams: [String: Any]?, completion: @escaping (_ result: RequestResponse?) -> Void) {
        
        // make the request
        networkRequest(action, apiParams: apiParams, completion: {(_ data: Data?) -> Void in
            guard let data = data else {
                completion(nil)
                return
            }
            
            do {
                let apiResponse = try JSONDecoder().decode(RequestResponse.self, from: data)
                guard let token = apiResponse.token else { return }
                if let defaults = UserDefaults(suiteName: kAppGroupIdentifier) {
                    defaults.set(token, forKey: kDeviceTokenKey)
                    defaults.synchronize()
                }
                completion(apiResponse)
            } catch let jsonError {
                print("Error serializing json", jsonError)
                completion(nil)
            }
        })
    }
    
    func dateFrom(string: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.date(from: string)
    }
    
    // sync the messages
    func synchronize() {
        networkRequest("list", apiParams: nil, completion: {(_ result: Data?) -> Void in
            // try and get the message
            guard let data = result else { return }
            do {
                let apiResponse = try JSONDecoder().decode(ListResponse.self, from: data)
                let defaults = UserDefaults.standard
                if let message = apiResponse.message, message != "" {
                    defaults.set(message, forKey: "message")
                }
                else {
                    defaults.removeObject(forKey: "message")
                }
                defaults.synchronize()
                
                // process requests
                guard let data = apiResponse.data else { return }
                for request in data {
                    self.updateDetails(request)
                    CoreDataManager.shared.saveContext()
                }
            } catch let jsonError {
                print("Error serializing json", jsonError)
                //completion(nil)
            }
        })
    }
    
    func updateDetails(_ request: ListData?) {
        guard let token = request?.token else { return }
        guard let message = Message.fetch(byToken: token) else { return }
        if let status = request?.status {
            message.status = status
        }
        guard request?.status == MessageState.messageAccepted.rawValue else { return }
        if let latitude = request?.latitude, let longitude = request?.longitude {
            message.latitude = latitude
            message.longitude = longitude
        }
        if let accuracy = request?.accuracy {
            message.accuracy = accuracy
        }
        if let dateString = request?.request_date, let serverDate = self.dateFrom(string: dateString) {
            if let answerString = request?.answer_date, let answerDate = self.dateFrom(string: answerString), let requestDate = message.requestDate {
                let diff: TimeInterval = requestDate.timeIntervalSince(serverDate)
                message.answerDate = answerDate.addingTimeInterval(diff)
            }
        }
    }
}

