//
//  ApiServiceHelper.swift
//  waua
//
//  Created by Edin on 10/25/17.
//  Copyright © 2017 qsdbih. All rights reserved.
//

import Foundation

extension ApiService {
    
    func networkRequest(_ action: String, apiParams: [String: Any]?, completion: @escaping (_ result: Data?) -> Void) {
        
        // generate the url
        var apiUrl = "\(API_URL)/\(action)"
        // get the params
        var params = apiRequestParams(apiParams)
        // generate salt
        #if API_SALT
            var saltParts = [String]()
            saltParts.append(API_SALT)
            saltParts.append(action)
            
            for item in params {
                guard let paramsName = params[item.key] as? String else { continue }
                saltParts.append(paramsName)
            }
            
            let salt = saltParts.joined(separator: "").md5()
            params["signature"] = salt
        #endif
        // add GET params
        if !params.isEmpty {
            apiUrl += encodeGetData(params)
        }
        // the URL
        guard let url = URL(string: apiUrl) else { return }
        print("url = \(url)")
        // the request object
        var request = URLRequest(url: url)
        request.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        request.timeoutInterval = 100
        
        let configuration = URLSessionConfiguration.ephemeral
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: OperationQueue.main)
        let task = session.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            guard let data = data else {
                print("""
                    API request failed: \(String(describing: request.url))
                    error = \(error.debugDescription)
                    """)
                
                completion(nil)
                return
            }
            completion(data)
        })
        task.resume()
    }
    
    // get the api params
    // app version, type, device token, notification token
    func apiRequestParams(_ apiParams: [String: Any]?) -> [String: Any] {
        var params = [String: Any]()
        var deviceToken: String?
        if let defaults = UserDefaults(suiteName: kAppGroupIdentifier) {
            deviceToken = defaults.object(forKey: kDeviceTokenKey) as? String
        }
        let notificationToken = notificationsToken()
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        params["version"] = version
        params["type"] = "iPhone"
        params["language"] = NSLocale.preferredLanguages[0]
        if deviceToken != nil {
            params["token"] = deviceToken
        }
        if notificationToken != "" {
            params["code"] = notificationToken
        }
        guard let apiParams = apiParams else { return params }
        for (key, _) in apiParams {
            params[key] = apiParams[key]
        }
        return params
    }
    
    // encode get data
    func encodeGetData(_ dictionary: [AnyHashable: Any]) -> String {
        var parts = [String]()
        for item in dictionary {
            if let key = item.key as? String {
                guard let value = dictionary[key] as? String else { continue }
                let encodedValue = value.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
                let encodedKey = key.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
                parts.append(encodedKey! + "=" + encodedValue!)
            }
        }
        return "?\(parts.joined(separator: "&"))"
    }
    
    // encode dictionary into a string
    func encodePostData(_ dictionary: [AnyHashable: Any]) -> Data {
        var parts = [String]()
        for item in dictionary {
            if let key = item.key as? String {
                guard let value = dictionary[key] as? String else { continue }
                let encodedValue = value.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
                let encodedKey = (key as NSString).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
                let part = encodedKey! + "=" + encodedValue!
                parts.append(part)
            }
        }
        let encodedDictionary: String = parts.joined(separator: "&")
        return encodedDictionary.data(using: String.Encoding.utf8) ?? Data()
    }
}
