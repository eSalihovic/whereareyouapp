//
//  AppStoreService.swift
//  waua
//
//  Created by Edin on 10/11/17.
//  Copyright © 2017 qsdbih. All rights reserved.
//

import Foundation
import StoreKit

// the pro user account product id
private let kProUserProductId = "waua_pro_user_autorenewable"
private let kProUserNonConsumableProductId = "waua_pro_user_nonconsumable"
private let kUnlockProUser = "waua_pro_user"

typealias AppStoreServiceCompletionHandler = (_ success: Bool, _ products: [SKProduct]?) -> ()
typealias AppStoreServiceConfirmationHandler = (_ status: Bool) -> ()

extension Notification.Name {
    // Notify the view controller to announce that the transaction is completed regardless of whether it is successful or not.
    static let inAppTransactionDidFinish = Notification.Name("iTunesTransactionDidFinish")
}

class AppStoreService: NSObject, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    
    // initialize
    override init() {
        super.init()
        
        // observe
        SKPaymentQueue.default().add(self)
    }

    static let shared = AppStoreService()

    private var productsRequest: SKProductsRequest?
    private var completionHandler: AppStoreServiceCompletionHandler?
    private var confirmationHandler: AppStoreServiceConfirmationHandler?
    private var products = [SKProduct]()

    // is this a pro user?
    class func isProUser() -> Bool {
        #if DEBUG
            return true
        #endif
        let defaults = UserDefaults.standard
        return defaults.bool(forKey: kProUserProductId) || defaults.bool(forKey: kProUserNonConsumableProductId)
    }
    
    
    // find the pro product
    var proProduct: SKProduct? {
        for product in products {
            if (product.productIdentifier == kProUserProductId) {
                return product
            }
        }
        return nil
    }

    // will first check if user is already a pro user and if not prompt user to
    // become pro
    class func becomeProUser(_ confirmationHandler: @escaping AppStoreServiceConfirmationHandler, useSuggestion suggest: Bool) {
        if isProUser() {
            confirmationHandler(true)
        }
        else {
            self.shared._becomeProUser(confirmationHandler, useSuggestion: suggest)
        }
    }

    class func priceString() -> String? {
        return self.shared.priceString()
    }

    // get price string
    func priceString() -> String? {
        guard let product = products.first else { return nil }
        let numberFormatter = NumberFormatter()
        numberFormatter.formatterBehavior = .behavior10_4
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = product.priceLocale
        let priceString = numberFormatter.string(from: product.price)
        return priceString
    }

    func startProductsRequest() {
        let productsRequest = SKProductsRequest(productIdentifiers: [kProUserProductId, kProUserNonConsumableProductId])
        productsRequest.delegate = self
        productsRequest.start()
    }
    
    // purchase private
    func _becomeProUser(_ confirmationHandler: @escaping AppStoreServiceConfirmationHandler, useSuggestion suggest: Bool) {
        self.confirmationHandler = confirmationHandler
        startProductsRequest()
        
        var message = ""
        guard let priceString = priceString() else { return }
        if suggest {
            message = String(format: NSLocalizedString("Subscription_Suggest", comment: ""), priceString)
        }
        else {
            message = """
            \(NSLocalizedString("Subscription_text", comment: ""))
            \(priceString)
            """
        }
        
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertController.view.tintColor = .wauaGreen()
        let okAction = UIAlertAction(title: NSLocalizedString("Subscription_OK", comment: ""), style: .default) { (alertAction) in
            self.getProAccount()
        }
        alertController.addAction(okAction)
        
        let restoreAction = UIAlertAction(title: NSLocalizedString("Restore", comment: ""), style: .default) { (alertAction) in
            SKPaymentQueue.default().restoreCompletedTransactions()
        }
        alertController.addAction(restoreAction)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Subscription_NO", comment: ""), style: .cancel) { (alertAction) in
            self.confirmPurchase(false)
            return
        }
        alertController.addAction(cancelAction)
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindowLevelAlert + 1;
        alertWindow.makeKeyAndVisible()
        alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    // alert view option selected
    func alertView(_ alertView: UIAlertController, didDismissWithButtonIndex buttonIndex: Int) {
        // cancel clicked?
        if buttonIndex == 0 {
            confirmPurchase(false)
            return
        }
        // no pro product?
        if proProduct == nil {
            confirmPurchase(false)
            return
        }
        // buy
        if buttonIndex == 1 {
            getProAccount()
        }
        else if buttonIndex == 2 {
            SKPaymentQueue.default().restoreCompletedTransactions()
        }
        else {
            print("something wrong. invalid button index")
            confirmPurchase(false)
        }

    }
    
    // restore completed
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        print("paymentQueueRestoreCompletedTransactionsFinished")
        if queue.transactions.count == 0 {
            NotificationCenter.default.post(name: .inAppTransactionDidFinish,
                                            object: nil,
                                            userInfo: ["isSuccess" : false,
                                                       "message": NSLocalizedString("Subcsription_NotFound", comment: "")])
            let alertController = UIAlertController(title: nil, message: NSLocalizedString("Subcsription_NotFound", comment: ""), preferredStyle: .alert)
            alertController.view.tintColor = .wauaGreen()
            let cancelAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
            alertWindow.rootViewController = UIViewController()
            alertWindow.windowLevel = UIWindowLevelAlert + 1;
            alertWindow.makeKeyAndVisible()
            alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
        }
        else {
            for transaction in queue.transactions {
                if transaction.transactionState == .restored {
                    SKPaymentQueue.default().finishTransaction(transaction)
                }
            }
        }
    }
    
    // get the pro account
    func getProAccount() {
        let proAccountProduct: SKProduct? = proProduct
        if proAccountProduct == nil {
            print("PRO Account Product not found!")
            NotificationCenter.default.post(name: .inAppTransactionDidFinish,
                                            object: nil,
                                            userInfo: ["isSuccess" : false,
                                                       "message": NSLocalizedString("PRO Account Product not found!", comment: "")])
            confirmPurchase(false)
            return
        }
        // can make payments?
        if !SKPaymentQueue.canMakePayments() {
            NotificationCenter.default.post(name: .inAppTransactionDidFinish,
                                            object: nil,
                                            userInfo: ["isSuccess" : false,
                                                       "message": NSLocalizedString("Can't make payments", comment: "")])
            print("can't make payments")
            confirmPurchase(false)
            return
        }
        guard let product = proAccountProduct else { return }
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
    }

    //
    // Failed to restore (network error, can't connect to App Store, user hit cancel, ...)
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        print("restoreCompletedTransactionsFailedWithError: \(error)")
        
        let alertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        alertController.view.tintColor = .wauaGreen()
        let cancelAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindowLevelAlert + 1;
        alertWindow.makeKeyAndVisible()
        alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
        
        // finish all transactions
        for transaction in queue.transactions {
            SKPaymentQueue.default().finishTransaction(transaction)
        }
        NotificationCenter.default.post(name: .inAppTransactionDidFinish,
                                        object: nil,
                                        userInfo: ["isSuccess" : false, "message": error.localizedDescription])
        
        confirmPurchase(false)
        
    }

    // product is purchased
    func provideContent(forProductIdentifier transaction: SKPaymentTransaction) {
        let productIdentifier = transaction.payment.productIdentifier
        print("Provide content for product: \(productIdentifier)")
        if (productIdentifier == kProUserProductId || productIdentifier == kProUserNonConsumableProductId) {
            // store data
            UserDefaults.standard.set(true, forKey: productIdentifier)
            UserDefaults.standard.synchronize()
            // success
            confirmPurchase(true)
        }
    }
    
    // call the confirmation callback and null it.
    func confirmPurchase(_ success: Bool) {
        // success
        confirmationHandler?(success)
        confirmationHandler = nil
        
    }
    
    // transaction complete
    func complete(_ transaction: SKPaymentTransaction) {
        print("completeTransaction...")
        provideContent(forProductIdentifier: transaction)
        SKPaymentQueue.default().finishTransaction(transaction)
        NotificationCenter.default.post(name: .inAppTransactionDidFinish,
                                        object: nil,
                                        userInfo: ["isSuccess" : true,
                                                   "message": NSLocalizedString("Success!", comment: "")])
    }
    
    // restoring existing one
    func restore(_ transaction: SKPaymentTransaction) {
        print("restoreTransaction")
        if let original = transaction.original {
            provideContent(forProductIdentifier: original)
        }
        else {
            provideContent(forProductIdentifier: transaction)
        }
        SKPaymentQueue.default().finishTransaction(transaction)
        NotificationCenter.default.post(name: .inAppTransactionDidFinish,
                                        object: nil,
                                        userInfo: ["isSuccess" : true,
                                                   "message": NSLocalizedString("Successfully Restored!", comment: "")])
    }
    
    // transaction failed
    func failedTransaction(_ transaction: SKPaymentTransaction) {
        // user cancelled?
        if let error = transaction.error {
            let alertController = UIAlertController(title: "Error", message: transaction.error?.localizedDescription, preferredStyle: .alert)
            alertController.view.tintColor = .wauaGreen()
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
            alertWindow.rootViewController = UIViewController()
            alertWindow.windowLevel = UIWindowLevelAlert + 1;
            alertWindow.makeKeyAndVisible()
            alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
            
            print("Transaction error: \(String(describing: error.localizedDescription))")
        }
        // finalize
        SKPaymentQueue.default().finishTransaction(transaction)
        NotificationCenter.default.post(name: .inAppTransactionDidFinish,
                                        object: nil,
                                        userInfo: ["isSuccess" : false,
                                                   "message": transaction.error?.localizedDescription ?? NSLocalizedString("Unknown error", comment: "")])
        // failed
        confirmPurchase(false)
    }
    
    // process the payment queue
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction: SKPaymentTransaction in transactions {
            switch transaction.transactionState {
            case .purchased:
                complete(transaction)
            case .failed:
                failedTransaction(transaction)
            case .restored:
                restore(transaction)
            default:
                break
            }
        }
    }

    //
    // get products
    func requestProducts(with completionHandler: @escaping AppStoreServiceCompletionHandler) {
        // 1
        self.completionHandler = completionHandler
        // 2
        productsRequest = SKProductsRequest(productIdentifiers: Set<String>([kProUserProductId, kProUserNonConsumableProductId]))
        productsRequest?.delegate = self
        productsRequest?.start()
    }

    //
    // process results
     func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print("Loaded list of products...")
        print("products: \(response.products)")
        productsRequest = nil
        for invalidProductId in response.invalidProductIdentifiers {
            print("Invalid product id: \(invalidProductId)")
        }
        products = response.products
        for skProduct in products {
            let numberFormatter = NumberFormatter()
            numberFormatter.formatterBehavior = .behavior10_4
            numberFormatter.numberStyle = .currency
            numberFormatter.locale = skProduct.priceLocale
            let formattedString = numberFormatter.string(from: skProduct.price)
            print("Found product: \(skProduct.productIdentifier) \(skProduct.localizedTitle) \(String(describing: formattedString))")
        }
        completionHandler?(true, products)
        completionHandler = nil
    }

    // failed
    func request(_ request: SKRequest, didFailWithError error: Error) {
        print("Failed to load list of products.")
        productsRequest = nil
        completionHandler?(false, nil)
        completionHandler = nil
    }
}
