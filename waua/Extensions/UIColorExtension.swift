//
//  UIColorExtension.swift
//  waua
//
//  Created by Edin on 11/9/17.
//  Copyright © 2017 qsdbih. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    class func wauaGreen() -> UIColor {
        return UIColor(red: 0.000, green: 0.600, blue: 0.000, alpha: 1)
    }
    
    class func lightBlue() -> UIColor {
        return UIColor(red: 0.000, green: 0.478, blue: 1.000, alpha: 1)
    }
}
