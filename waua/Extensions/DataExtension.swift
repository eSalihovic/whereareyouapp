//
//  DataExtension.swift
//  waua
//
//  Created by Edin on 11/15/17.
//  Copyright © 2017 qsdbih. All rights reserved.
//

import Foundation

extension Data {
    func hexString() -> String {
        let nsdataStr = NSData.init(data: self)
        return nsdataStr.description.trimmingCharacters(in: CharacterSet(charactersIn: "<>")).replacingOccurrences(of: " ", with: "")
    }
}
