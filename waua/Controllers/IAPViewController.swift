//
//  IAPViewController.swift
//  waua
//
//  Created by qsdbih on 1/10/18.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit
import StoreKit

class IAPViewController: UIViewController {
    
    @IBOutlet var dimView: UIView!
    @IBOutlet var buyProView: UIView!
    @IBOutlet var paymentPolicyView: UIView!
    @IBOutlet var buyProButtonView: UIView!
    @IBOutlet var proPriceLabel: UILabel!
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet weak var aiWaitingInApp: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        aiWaitingInApp.hidesWhenStopped = true
        aiWaitingInApp.activityIndicatorViewStyle = .whiteLarge
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(onInAppTransactionDidFinish),
                                               name: .inAppTransactionDidFinish,
                                               object: nil)
        dimView.alpha = 0
        buyProView.layer.cornerRadius = 10
        paymentPolicyView.layer.cornerRadius = 10
        buyProButtonView.layer.cornerRadius = buyProButtonView.frame.height / 2
        cancelButton.tintColor = .wauaGreen()
        proPriceLabel.text = AppStoreService.priceString() ?? ""
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.dimView.alpha = 0.7
        }, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .inAppTransactionDidFinish, object: nil)
        UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.dimView.alpha = 0
        }, completion: nil)
    }
    
    @IBAction func buyProTapped(_ sender: Any) {
        //UIHelper.showHUD(self, message: NSLocalizedString("Please wait...", comment: ""))
        showWaitingInAppActivityIndicator()
        AppStoreService.shared.self.getProAccount()
    }
    
    @IBAction func restoreTapped(_ sender: Any) {
        //UIHelper.showHUD(self, message: NSLocalizedString("Please wait...", comment: ""))
        showWaitingInAppActivityIndicator()
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    
    @IBAction func cancelTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func paymentPolicyTapped(_ sender: Any) {
        let transitionOptions: UIViewAnimationOptions = [.transitionFlipFromRight, .showHideTransitionViews]
        
        UIView.transition(with: buyProView, duration: 0.3, options: transitionOptions, animations: {
            self.buyProView.isHidden = true
        })
        
        UIView.transition(with: paymentPolicyView, duration: 0.3, options: transitionOptions, animations: {
            self.paymentPolicyView.isHidden = false
            self.cancelButton.tintColor = .lightBlue()
        })
    }
    
    @IBAction func paymentPolicyBackTapped(_ sender: Any) {
        let transitionOptions: UIViewAnimationOptions = [.transitionFlipFromLeft, .showHideTransitionViews]
        
        UIView.transition(with: buyProView, duration: 0.3, options: transitionOptions, animations: {
            self.paymentPolicyView.isHidden = true
        })
        
        UIView.transition(with: paymentPolicyView, duration: 0.3, options: transitionOptions, animations: {
            self.buyProView.isHidden = false
            self.cancelButton.tintColor = .wauaGreen()
        })
    }
    
    fileprivate func showWaitingInAppActivityIndicator() {
        aiWaitingInApp.startAnimating()
        aiWaitingInApp.isHidden = false
    }
    
    fileprivate func hideWaitingInAppActivityIndicator() {
        aiWaitingInApp.stopAnimating()
    }
    
    @objc open func onInAppTransactionDidFinish(_ notification: NSNotification) {
        //UIHelper.hideHUD(self)
        hideWaitingInAppActivityIndicator()
        guard let userInfoDict = notification.userInfo as? [String:Any],
            let isSuccess = userInfoDict["isSuccess"] as? Bool,
            let _ = userInfoDict["message"] as? String else {
                return
        }
        
        if isSuccess {
            dismiss(animated: true, completion: nil)
        }
        //        else {
        //            let alertVC = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        //            alertVC.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
        //            self.present(alertVC, animated: true, completion: nil)
        //        }
    }
}
