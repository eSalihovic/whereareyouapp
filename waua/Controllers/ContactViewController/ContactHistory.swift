//
//  ContactHistory.swift
//  waua
//
//  Created by Edin on 12/13/17.
//  Copyright © 2017 qsdbih. All rights reserved.
//

import Foundation
import UIKit

private let StoreKey = "recent.requests"
private typealias StoreType = [Dictionary<String, String>]
private let MaxItems = 5;
private let NameKey = "name"
private let PhoneKey = "phone"

class HistoryItem : NSObject {
    var name : String
    var phone : String
    
    init(withName name : String, andPhone phone: String) {
        self.name = name
        self.phone = phone
    }
}

class ContactHistory : NSObject {
    // History items
    var history : [HistoryItem] = []
    
    override init() {
        super.init()
        load();
    }
    
    // push item to the history
    func addItem(item : HistoryItem) {
        // remove this item if it is already in history
        let phone = cleanPhone(phone: item.phone)
        for index in 0 ..< history.count {
            let old  = history[index]
            if cleanPhone(phone: old.phone) == phone {
                history.remove(at: index)
                break;
            }
        }
        
        // add the element
        history.insert(item, at: 0);
        
        // ensure max length is not exceeded
        while history.count > MaxItems {
            history.removeLast()
        }
        
        // and save
        save()
    }
    
    // remove spaces from a phone nymber
    private func cleanPhone(phone : String) -> String {
        return phone.replacingOccurrences(of: "\\s+", with: "")
    }
    
    private func save() {
        let defaults = UserDefaults.standard
        
        var arr = StoreType();
        for item in history {
            arr.append([
                NameKey : item.name,
                PhoneKey: item.phone
                ])
        }
        
        defaults.set(arr, forKey: StoreKey)
        defaults.synchronize()
    }
    
    private func load() {
        let defaults = UserDefaults.standard
        let arr = defaults.object(forKey: StoreKey) as? StoreType ?? StoreType()
        
        for item in arr {
            history.append(HistoryItem(
                withName: item[NameKey]!,
                andPhone: item[PhoneKey]!
            ))
        }
    }
}
