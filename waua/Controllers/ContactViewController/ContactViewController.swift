//
//  ContactViewController.swift
//  waua
//
//  Created by Edin on 9/28/17.
//  Copyright © 2017 qsdbih. All rights reserved.
//

import UIKit
import ContactsUI
import MessageUI

class ContactViewController: UIViewController, UINavigationControllerDelegate, CNContactPickerDelegate, MFMessageComposeViewControllerDelegate {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var stepImages: [UIImageView]!
    @IBOutlet var stepLabels: [UILabel]!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var locateButton: UIButton!
    
    private var loadingView: UIView!
    var message = [String: Any]()
    var _history: ContactHistory?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = NSLocalizedString("InstructionPage_Heading", comment: "")
        descriptionLabel.text = NSLocalizedString("InstructionPage_LocationService", comment: "")
        locateButton.setTitle(NSLocalizedString("InstructionPage_Button", comment: ""), for: .normal)
        for i in 0..<3 {
            let label = stepLabels[i]
            let key = "InstructionPage_Step\(i + 1)"
            label.text = NSLocalizedString(key, comment: "")
        }
        
        // locate somebody button background
        var btnImage = UIImage(named: "button_outline")
        guard let image = btnImage else { return }
        btnImage = image.resizableImage(withCapInsets: UIEdgeInsetsMake(5, 5, 5, 5))
        locateButton.setBackgroundImage(btnImage, for: .normal)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        layoutIntro()
    }
    
    // become a pro
    func becomePro() {
        AppStoreService.becomeProUser({(_ yes: Bool) -> Void in
        }, useSuggestion: true)
    }
    
    var history: ContactHistory {
        if _history == nil {
            _history = ContactHistory()
        }
        return _history!
    }
    
    private func instantiateIAPVC() -> UIViewController? {
        return storyboard?.instantiateViewController(withIdentifier: "IAPVC") as? IAPViewController
        
    }
    
    func pushIAPVC() {
        if let iapVC = instantiateIAPVC() {
            let window = UIWindow(frame: UIScreen.main.bounds)
            window.rootViewController = UIViewController()
            window.windowLevel = UIWindowLevelAlert + 1;
            window.makeKeyAndVisible()
            window.rootViewController?.present(iapVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func locateButtonTapped(_ sender: Any) {
        
        let pro = AppStoreService.isProUser()
        let title = NSLocalizedString("ContactSource_Title", comment: "")
        let addressBook = NSLocalizedString("ContactSource_AddressBook", comment: "")
        let recentHistory = NSLocalizedString(pro ? "ContactSource_RecentHistory" : "ContactSource_RecentHistory_Locked", comment: "")
        let phoneNumber = NSLocalizedString(pro ? "ContactSource_PhoneNumber" : "ContactSource_PhoneNumber_Locked", comment: "")
        let cancel = NSLocalizedString("ContactSource_Cancel", comment: "")

        let alertController = UIAlertController(title: title,
                                                message: nil,
                                                preferredStyle: .actionSheet)
        alertController.view.tintColor = .wauaGreen()
        let contactsAction = UIAlertAction(title: addressBook, style: .default) { (alertAction) in
            AppDelegate.shared.checkAccessStatus(completionHandler: { [weak self] (accessGranted) -> Void in
                guard let strongSelf = self else { return }
                if accessGranted {
                    let contactPicker = strongSelf.contactPickerViewController()
                    strongSelf.present(contactPicker, animated: true, completion: nil)
                }
                else {
                    strongSelf.showContactsAccessDeniedAlert()
                }
            })
        }

        let recentAction = UIAlertAction(title: recentHistory, style: .default) { (alertAction) in
            if AppStoreService.isProUser() {
                self.contactFromHistory()
            }
            else {
                self.pushIAPVC()
            }
        }

        let manualyAction = UIAlertAction(title: phoneNumber, style: .default) { (alertAction) in
            if AppStoreService.isProUser() {
                self.contactUsingPhoneNumber()
            }
            else {
                self.pushIAPVC()
            }
        }

        let cancelAction = UIAlertAction(title: cancel, style: .cancel, handler: nil)

        alertController.addAction(contactsAction)
        alertController.addAction(recentAction)
        alertController.addAction(manualyAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func contactUsingPhoneNumber() {
        let title = NSLocalizedString("PhoneNumber_Title", comment: "")
        let message = NSLocalizedString("PhoneNumber_Description", comment: "")
        let actionTitle = NSLocalizedString("PhoneNumber_Send", comment: "")
        let directNumberAC = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        directNumberAC.view.tintColor = .wauaGreen()
        
        let locateAction = UIAlertAction(title: actionTitle, style: .default) { (alertAction) in
            guard let name = directNumberAC.textFields?.first?.text,
                let phone = directNumberAC.textFields?.last?.text else { return }
            self.sendRequest(name, phone: phone)
        }
        
        directNumberAC.addTextField { (nameField : UITextField!) -> Void in
            nameField?.placeholder = NSLocalizedString("PhoneNumber_Name", comment: "")
            nameField?.isSecureTextEntry = false
            nameField?.autocapitalizationType = .words
        }
        
        directNumberAC.addTextField { (phoneField : UITextField!) -> Void in
            phoneField?.keyboardType = .phonePad
            phoneField?.placeholder = NSLocalizedString("PhoneNumber_Phone", comment: "")
            //phoneField?.isSecureTextEntry = false
        }
        
        let phoneField: UITextField? = directNumberAC.textFields?.last
        phoneField?.keyboardType = .phonePad
        phoneField?.placeholder = NSLocalizedString("PhoneNumber_Phone", comment: "")
        directNumberAC.addAction(locateAction)
        present(directNumberAC, animated: true, completion: nil)
    }
    
    // pick contact from the recent history
    
    func contactFromHistory() {
        let title = NSLocalizedString("Recents_Title", comment: "")
        let cancel = NSLocalizedString("Recents_Cancel", comment: "")
        let contactFromHistoryAC = UIAlertController(title: title,
                                               message: nil,
                                               preferredStyle: .actionSheet)
        contactFromHistoryAC.view.tintColor = .wauaGreen()
        
        for item in history.history {
            let action = UIAlertAction(title: "\(item.name) (\(item.phone))", style: .default) { (alertAction) in
                self.sendRequest(item.name, phone: item.phone)
            }
            contactFromHistoryAC.addAction(action)
        }
        
        let cancelAction = UIAlertAction(title: cancel, style: .cancel, handler: nil)
        contactFromHistoryAC.addAction(cancelAction)
        
        present(contactFromHistoryAC, animated: true, completion: nil)
    }

    
    func showContactsAccessDeniedAlert() {
        let alertController = UIAlertController(title: nil,
                                                message: "WhereAreYou doesn't have access to your contacts. Please enable access in the Settings and turn on Contacts to continue.",
                                                preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (alertAction) in
            
            if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(appSettings, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(settingsAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    
    func contactPickerViewController() -> CNContactPickerViewController {
        let contactPicker = CNContactPickerViewController()
        contactPicker.displayedPropertyKeys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
        contactPicker.delegate = self
        contactPicker.view.tintColor = .wauaGreen()
        
        return contactPicker
    }
    
    func findPhoneNumbers(contact: CNContact) -> [String] {
        var phoneNumbers = [String]()
        
        for number: CNLabeledValue in contact.phoneNumbers {
            let numberValue = number.value
            guard let phone = numberValue.value(forKey: "digits") as? String else { continue }
            phoneNumbers.append(phone)
        }
        
        return phoneNumbers
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        showLoader(animated: true)
        let name = contact.givenName
        var phone: String?
        var phoneNumbers = findPhoneNumbers(contact: contact)
        
        if phoneNumbers.count == 0 {
            let alertController = UIAlertController(title: String(format: NSLocalizedString("NoPhoneNumberTitle", comment: ""), contact.givenName),
                                                    message: NSLocalizedString("NoPhoneNumberMessage", comment: ""), preferredStyle: .alert)
            alertController.view.tintColor = .wauaGreen()
            let cancelAction = UIAlertAction(title: NSLocalizedString("NoPhoneNumberCancel", comment: ""), style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            
            picker.dismiss(animated: true, completion: {
                self.hideLoader(animated: true)
                self.present(alertController, animated: true, completion: nil)
            })
        }
            
        else if phoneNumbers.count == 1 {
            phone = phoneNumbers[0]
            sendRequest(name, phone: phone)
        }
        else if phoneNumbers.count > 1 {
            let alertController = UIAlertController(title: String(format: NSLocalizedString("SelectPhoneNumberTitle", comment: ""), contact.givenName),
                                                    message: nil, preferredStyle: .actionSheet)
            alertController.view.tintColor = .wauaGreen()
            for number in phoneNumbers {
                let action = UIAlertAction(title: number, style: .default) { (alertAction) in
                    phone = number
                    self.sendRequest(name, phone: phone)
                }
                alertController.addAction(action)
            }
            
            let cancelAction = UIAlertAction(title: NSLocalizedString("SelectPhoneNumberCancel", comment: ""), style: .cancel) { (alertAction) in
                self.hideLoader(animated: true)
            }
            
            alertController.addAction(cancelAction)
            picker.dismiss(animated: true, completion: {
                self.present(alertController, animated: true, completion: nil)
            })
        }
    }
    
    // send the notification
    func sendRequest(_ name: String, phone: String?) {
        // get the request token from the server
        // because we use the answerTime to calculate how long ago user
        // accepted / rejected the request we need to know how big is the
        // server and client time difference. we use requestDate
        // difference for this.
        // once request is complete we increase the requestTime by adding
        // the time difference / 2.
        let requestTime = Date()
        // perform the request
        guard let phone = phone else { return }
        let params = ["name": name, "phone": phone]
        ApiService.shared.getRequest("request", apiParams: params, completion: { [weak self] (_ result: RequestResponse?) -> Void in
            //Flurry.logEvent("contact.compose-sms", withParameters: params, timed: true)
            let now = Date()
            let diff: TimeInterval = now.timeIntervalSince(requestTime)
            
            guard let requestToken = result?.request, let url = result?.url else { return }
            guard let strongSelf = self else { return }
            strongSelf.message = ["token": requestToken,            // the requets token !!! NOT device token
                "name": name, "phone": phone, "requestDate": requestTime.addingTimeInterval(diff / 2)]
            
            let defaults = UserDefaults.standard
            var message = defaults.object(forKey: "message") as? String
            if message == nil {
                message = NSLocalizedString("SmsMessageText", comment: "")
            }
            let messageText = String(format: message!, name, url)
            
            let pro = AppStoreService.isProUser()

            let alertController = UIAlertController(title: nil,
                                                    message: nil,
                                                    preferredStyle: .actionSheet)
            alertController.view.tintColor = .wauaGreen()
            let iMessageAction = UIAlertAction(title: "iMessage", style: .default) { (alertAction) in
                // SMS window
                let controller = MFMessageComposeViewController()
                if MFMessageComposeViewController.canSendText() {

                    controller.body = messageText
                    controller.recipients = [phone]
                    controller.messageComposeDelegate = self
                    controller.view.tintColor = .wauaGreen()

                    strongSelf.present(controller, animated: true, completion: {() -> Void in
                        strongSelf.hideLoader(animated: false)
                    })
                }
                else {
                    strongSelf.hideLoader(animated: true)
                }
            }
            //Add translations for other languages
            let moreTitle = NSLocalizedString(pro ? "More" : "More_Locked", comment: "")
            let moreAction = UIAlertAction(title: moreTitle, style: .default) { (alertAction) in
                if AppStoreService.isProUser() {
                    let activityVC = UIActivityViewController(activityItems: [messageText], applicationActivities: nil)
                    activityVC.popoverPresentationController?.sourceView = strongSelf.view
                    strongSelf.present(activityVC, animated: true, completion: nil)
                    activityVC.completionWithItemsHandler = { activity, success, items, error in
                        if activity?.rawValue != nil {
                            strongSelf.saveSentMessage(requestToken)
                        }
                        strongSelf.hideLoader(animated: true)
                    }
                }
                else {
                    strongSelf.hideLoader(animated: true)
                    strongSelf.pushIAPVC()
                }
            }

            let cancelAction = UIAlertAction(title: NSLocalizedString("SmsSendFailCancel", comment: ""), style: .cancel) { (alertAction) in
                strongSelf.hideLoader(animated: true)
            }

            alertController.addAction(cancelAction)
            alertController.addAction(iMessageAction)
            alertController.addAction(moreAction)
            strongSelf.present(alertController, animated: true, completion: nil)
        })
    }
    
    // done
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: {
            // request token
            guard let token = self.message["token"] as? String else { return }
            // sms sent?
            if result == .cancelled || result == .failed {
                //Flurry.endTimedEvent("contact.compose-sms", withParameters: nil)
                // remove from the server
                ApiService.shared.networkRequest("cancel", apiParams: ["request": token], completion: {(_ result: Data?) -> Void in})
                // remove message from local storage
                self.message.removeAll()
                // was this a failed request?
                if result == .failed {
                    //Flurry.logEvent("contact.sms-send-failed")
                    let alertController = UIAlertController(title: NSLocalizedString("SelectPhoneNumberCancel", comment: ""),
                                                            message: NSLocalizedString("SmsSendFailMessage", comment: ""), preferredStyle: .alert)
                    
                    let cancelAction = UIAlertAction(title: NSLocalizedString("SmsSendFailCancel", comment: ""), style: .cancel, handler: nil)
                    alertController.addAction(cancelAction)
                    self.present(alertController, animated: true, completion: nil)
                }
                else {
                    //Flurry.logEvent("contact.sms-send-cancelled")
                }
            }
            else if result == .sent {
                // get existing message and reuse it or create a new one
                self.saveSentMessage(token)
            }
        })
    }
    
    func saveSentMessage(_ token: String) {
        var message = Message.fetch(byToken: token)
        if message == nil {
            message = CoreDataManager.shared.createMessage()
            message?.token = token
        }
        else {
            message?.accuracy = 0.0
            message?.latitude = 0.0
            message?.longitude = 0.0
        }
        message?.status = MessageState.messageSent.rawValue
        message?.name = self.message["name"] as? String
        message?.number = self.message["phone"] as? String
        if let date = self.message["requestDate"] as? Date {
            message?.requestDate = date
        }
        
        // save and done
        CoreDataManager.shared.saveContext()
        guard let name = message?.name, let phone = message?.number else { return }
        // store in recent history
        let item = HistoryItem(withName: name, andPhone: phone)
        self.history.addItem(item: item)
        self.message.removeAll()
    }
}

private extension ContactViewController {
    
    // resize all labels and get the content height
    func layoutIntro() {
        let screenWidth = view.bounds.size.width
        let screenHeight = view.bounds.size.height - 50
        var yPos: CGFloat = 10
        let pad: CGFloat = 30
        let pad2x: CGFloat = pad * 2
        
        // support for the Arabic language
        if NSLocale.preferredLanguages[0].range(of:"ar") != nil {
            view.layer.setAffineTransform(CGAffineTransform(scaleX: -1, y: 1))
            titleLabel.layer.setAffineTransform(CGAffineTransform(scaleX: -1, y: 1))
            descriptionLabel.layer.setAffineTransform(CGAffineTransform(scaleX: -1, y: 1))
            locateButton.layer.setAffineTransform(CGAffineTransform(scaleX: -1, y: 1))
        }
        
        // resize all labels and get the content height
        var height: CGFloat = 0
        height += sizeLabel(titleLabel, maxWidth: screenWidth - pad2x)
        for i in 0..<3 {
            let label = stepLabels[i]
            let h = sizeLabel(label, maxWidth: screenWidth - pad2x - 10 - stepImages[0].frame.size.width)
            height += max(h, stepImages[i].frame.size.height)
        }
        height += sizeLabel(descriptionLabel, maxWidth: screenWidth - pad2x)
        
        // the button
        height += locateButton.frame.size.height
        var space: CGFloat = 0
        if height > screenHeight {
            space = 10
        }
        else {
            space = floor((screenHeight - height) / 7)
            if space < 5 {
                space = 10
            }
        }
        
        // position title
        var frame = CGRect.zero
        frame = titleLabel.frame
        frame.origin.y = yPos + space
        titleLabel.frame = frame
        yPos = frame.maxY
        
        // position steps
        for i in 0..<3 {
            let label = stepLabels[i]
            let icon = stepImages[i]
            
            // support for the Arabic language
            if NSLocale.preferredLanguages[0].range(of:"ar") != nil {
                label.layer.setAffineTransform(CGAffineTransform(scaleX: -1, y: 1))
            }
            
            let iconSize = icon.frame.size
            let labelSize = label.frame.size
            if iconSize.height > labelSize.height {
                frame = icon.frame
                frame.origin.y = yPos + space
                icon.frame = frame
                yPos = frame.maxY
                let y = frame.origin.y + (iconSize.height - labelSize.height) / 2
                frame = label.frame
                frame.origin.y = y
                label.frame = frame
            }
            else {
                frame = label.frame
                frame.origin.y = yPos + space
                label.frame = frame
                yPos = frame.maxY
                let y = frame.origin.y + (labelSize.height - iconSize.height) / 2
                frame = icon.frame
                frame.origin.y = y
                icon.frame = frame
            }
        }
        
        // position the description
        frame = descriptionLabel.frame
        frame.origin.y = yPos + space
        descriptionLabel.frame = frame
        descriptionLabel.center.x = scrollView.center.x
        yPos = frame.maxY
        
        // position the button
        frame = locateButton.frame
        frame.origin.y = yPos + space
        locateButton.frame = frame
        locateButton.center.x = scrollView.center.x
        yPos = frame.maxY
        
        // make screen scrollable?
        scrollView.contentSize = CGSize(width: screenWidth, height: yPos + space)
    }
    
    // size the label
    func sizeLabel(_ label: UILabel, maxWidth width: CGFloat) -> CGFloat {
        let size: CGSize = self.size(for: label, maxWidth: width)
        label.numberOfLines = Int((size.height / label.font.lineHeight).rounded(.down))
        var frame: CGRect = label.frame
        frame.size = size
        label.frame = frame
        return size.height
    }
    
    // calculate
    func size(for label: UILabel, maxWidth width: CGFloat) -> CGSize {
        var size: CGSize
        
        let rect = label.attributedText!.boundingRect(with: CGSize(width: width, height: 999), options: .usesLineFragmentOrigin, context: nil)
        size = rect.size
        
        size.width = ceil(size.width)
        size.height = ceil(size.height)
        return size
    }
    
    // get the loading view
    func getLoadingView() -> UIView {
        if loadingView == nil {
            loadingView = UIView(frame: view.bounds)
            loadingView.backgroundColor = UIColor.black.withAlphaComponent(0.75)
            loadingView.autoresizesSubviews = true
            loadingView.autoresizingMask = [.flexibleLeftMargin, .flexibleWidth, .flexibleRightMargin, .flexibleTopMargin, .flexibleHeight, .flexibleBottomMargin]
            let activity = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
            activity.center = CGPoint(x: loadingView.bounds.size.width / 2, y: loadingView.bounds.size.height / 2)
            activity.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin, .flexibleTopMargin, .flexibleBottomMargin]
            activity.startAnimating()
            loadingView.addSubview(activity)
            view.addSubview(loadingView)
        }
        loadingView.frame = view.bounds
        return loadingView
    }
    
    // show loading view
    func showLoader(animated: Bool) {
        loadingView = getLoadingView()
        if animated {
            loadingView.alpha = 0.0
            loadingView.isHidden = false
            UIView.animate(withDuration: 0.250, animations: {() -> Void in
                self.loadingView.alpha = 1.0
            })
        }
        else {
            loadingView.alpha = 1.0
            loadingView.isHidden = false
        }
    }
    
    // hide loading view
    func hideLoader(animated: Bool) {
        guard let _ = loadingView else { return } 
        if animated {
            UIView.animate(withDuration: 0.250, animations: {() -> Void in
                self.loadingView.alpha = 0.0
            }, completion: {(_ finished: Bool) -> Void in
                self.loadingView.isHidden = true
            })
        }
        else {
            loadingView.isHidden = true
        }
    }
}
