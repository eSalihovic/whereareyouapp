//
//  TabBarViewController.swift
//  waua
//
//  Created by Edin on 9/27/17.
//  Copyright © 2017 qsdbih. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBar.tintColor = .wauaGreen()
        
        guard let items = tabBar.items else { return }
        for item in items {
            var key = ""
            if item.tag == 0 {
                key = "TabContact"
            }
            else if item.tag == 1 {
                key = "TabMap"
            }
            else if item.tag == 2 {
                key = "TabHistory"
            }
            
            item.title = NSLocalizedString(key, comment: "")
        }
    }
}
