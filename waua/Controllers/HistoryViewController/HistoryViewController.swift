//
//  HistoryViewController.swift
//  waua
//
//  Created by Edin on 10/11/17.
//  Copyright © 2017 qsdbih. All rights reserved.
//

import Foundation
import UIKit
import CoreData

let kMapSegue = "map"
let kCellIdentifier = "message"

class HistoryViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var emptyView: UIView!
    @IBOutlet var emptyLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let timer = Timer(timeInterval: 1.0, target: self, selector: #selector(self.updateTime), userInfo: nil, repeats: true)
        RunLoop.main.add(timer, forMode: .defaultRunLoopMode)
        emptyLabel.text = NSLocalizedString("HistoryEmptyList", comment: "")
        emptyView.isHidden = tableView(tableView, numberOfRowsInSection: 0) != 0
        tableView.reloadData()
    }
    
    // Navigate to contacts
    @IBAction func go(toContacts sender: Any) {
        navigationController?.tabBarController?.selectedIndex = 0
    }

    // update the time
    @objc func updateTime() {
        for cell in tableView.visibleCells {
            guard let index = tableView.indexPath(for: cell) else { continue }
            configureCell(cell, at: index)
        }
    }
    
    // get the managed object context
    var managedObjectContext: NSManagedObjectContext? {
        return CoreDataManager.shared.context
    }
    
    // results fetcher
    var fetchedResultsController: [Message]? {
        var messages: [Message]?
        guard let context = managedObjectContext else { return nil }
        let fetchRequest: NSFetchRequest<Message> = Message.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "requestDate", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        do {
            messages = try context.fetch(fetchRequest)
        } catch {
            fatalError("Failed to fetch message: \(error)")
        }
        
        return messages
    }
    
    // configure the cell
    func configureCell(_ cell: UITableViewCell, at indexPath: IndexPath) {
        guard let message = fetchedResultsController?[indexPath.row] else { return }//.object(at: indexPath)
        //print(message.accuracy)
        //let message = messages[indexPath.row]
        cell.textLabel?.text = message.name
        guard let status = message.status else { return }
        switch MessageState(rawValue: status) {
        case .messageSent?, .messageTimeout?:
            cell.detailTextLabel?.text = NSLocalizedString("HistoryNotAcceptedYet", comment: "")
            // "hasn't clicked the link yet";
            cell.imageView?.image = UIImage(named: "status_gray")
        case .messageAccepted?:
            guard let answerString = message.timeSpan() else {
                cell.detailTextLabel?.text = nil
                return
            }
            cell.detailTextLabel?.text = String(format: NSLocalizedString("HistorySharedLocation", comment: ""), answerString)
            cell.imageView?.image = UIImage(named: "status_green")
            cell.detailTextLabel?.text = String(format: NSLocalizedString("HistorySharedLocation", comment: ""), answerString)
        case .messageDenied?:
            guard let answerString = message.timeSpan() else {
                cell.detailTextLabel?.text = nil
                return
            }
            cell.detailTextLabel?.text = String(format: NSLocalizedString("HistoryRefusedSharing", comment: ""), answerString)
            cell.imageView?.image = UIImage(named: "status_red")
        case .messageDeleted?:
            cell.imageView?.image = UIImage(named: "status_red")
        default:
            break
        }
        if message.latitude != 0 && message.longitude != 0 {
            cell.accessoryType = .detailDisclosureButton
        } else {
            cell.accessoryType = .none
        }
    }
    
    // prepeare
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == kMapSegue,
            let mapVC = segue.destination as? MapViewController,
            let message = sender as? Message else { return }
        mapVC.isFromHistoryVC = true
        mapVC.centerLocation = message
    }
    
    // end update
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
        emptyView.isHidden = tableView(tableView, numberOfRowsInSection: 0) != 0
    }
}

extension HistoryViewController: UITableViewDataSource, UITableViewDelegate {
    
    // number of items in a section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return fetchedResultsController?.count ?? 0
    }
    
    // get the cell for the index
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // get the cell
        let cell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier, for: indexPath)
        // configure it
        configureCell(cell, at: indexPath)
        // done
        return cell
    }
    
    // Override to support conditional editing of the table view.
    // This only needs to be implemented if you are going to be returning NO
    // for some items. By default, all items are editable.
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return YES if you want the specified item to be editable.
        return true
    }
    
    //
    // Override to support editing the table view.
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            guard let messages = fetchedResultsController else { return }//?.object(at: indexPath)
            let message = messages[indexPath.row]
            //Flurry.logEvent("history.delete", withParameters: ["name": message?.name, "phone": message?.number])
            ApiService.shared.networkRequest("delete", apiParams: ["request": message.token as Any], completion: {(_ result: Data?) -> Void in})
            managedObjectContext?.delete(message)
            try? managedObjectContext?.save()
            tableView.deleteRows(at: [indexPath], with: .fade)
            emptyView.isHidden = self.tableView(tableView, numberOfRowsInSection: 0) != 0
        }
        else if editingStyle == .insert {
            tableView.insertRows(at: [indexPath], with: .fade)
        }
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    // perform selection
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let messages = fetchedResultsController else { return }//?.object(at: indexPath)
        let message = messages[indexPath.row]
        self.tableView.deselectRow(at: indexPath, animated: true)
        if message.latitude != 0 && message.longitude != 0 {
            performSegue(withIdentifier: "map", sender: message)
        }
    }
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        self.tableView(tableView, didSelectRowAt: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        guard let messages = fetchedResultsController else { return nil }
        let message = messages[indexPath.row]
        if message.latitude != 0 && message.longitude != 0 {
            return indexPath
        }
        else {
            return nil
        }
    }
    
    // don't highlight the row if it doesn't have a location
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
//        guard let message = fetchedResultsController?.object(at: indexPath) else { return false }
//        return message.hasLocation()
        guard let messages = fetchedResultsController else { return false }
        let message = messages[indexPath.row]
        if message.latitude != 0 && message.longitude != 0 {
            return true
        }
        else {
            return false
        }
    }
}
