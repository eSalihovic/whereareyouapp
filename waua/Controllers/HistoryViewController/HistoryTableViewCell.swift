//
//  HistoryTableViewCell.swift
//  waua
//
//  Created by Edin on 10/11/17.
//  Copyright © 2017 qsdbih. All rights reserved.
//

import Foundation
import UIKit

class HistoryTableViewCell: UITableViewCell {
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var statusView: UIImageView!
    
    override func prepareForReuse() {
        nameLabel.text = nil
        descriptionLabel.text = nil
        statusView.image = nil
    }
}
