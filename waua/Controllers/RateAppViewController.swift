//
//  RateAppViewController.swift
//  waua
//
//  Created by qsdbih on 5/3/18.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit
import StoreKit

class RateAppViewController: UIViewController {
    
    @IBOutlet weak var vwDimBackgroundView: UIView!
    @IBOutlet weak var vwRateAppContentParentView: UIView!
    @IBOutlet weak var imgvwStars: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnRate5Stars: UIButton!
    @IBOutlet weak var btnNoThanks: UIButton!
    
    var isManualLaunch = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        vwRateAppContentParentView.layer.cornerRadius = 7
        lblTitle.text = NSLocalizedString("rate_dialog_title", comment: "")
        let message = NSLocalizedString("rate_dialog_content", comment: "")
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 3
        let attributes = [NSAttributedStringKey.paragraphStyle:paragraphStyle]
        lblMessage.attributedText = NSAttributedString(string: message, attributes: attributes)
        btnRate5Stars.setTitle(NSLocalizedString("five_stars", comment: "").uppercased(), for: .normal)
        btnNoThanks.setTitle(NSLocalizedString("no_thanks", comment: "").uppercased(), for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        vwDimBackgroundView.alpha = 0
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.155, delay: 0.0, options: .curveLinear, animations: {
            self.vwDimBackgroundView.alpha = 0.4
        }, completion: nil)
    }
    
    
    @IBAction func rate5StarsButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
        func openAppStore() {
            print("Opening AppStore")
            let iTunesURL = URL(string: "itms-apps://itunes.apple.com/app/id608238999")!
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(iTunesURL, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(iTunesURL)
            }
        }
        
        func showSKStoreReviewControllerIfAvailable_OtherwiseOpenAppStore() {
            if #available( iOS 10.3,*) {
                print("Opening SKStoreReviewController")
                SKStoreReviewController.requestReview()
            } else {
                print("SKStoreReviewController is not available for current iOS version")
                openAppStore()
            }
        }
        
        
        if isManualLaunch {
            let skStoreReviewController_manuallyLaunchOnce = UserDefaults.standard.bool(forKey: "SKStoreReviewController_manuallyLaunchOnce")
            let userPressedRate5StarsBefore = UserDefaults.standard.bool(forKey: "user_pressed_rate_5_stars")
            if !skStoreReviewController_manuallyLaunchOnce && !userPressedRate5StarsBefore {
                UserDefaults.standard.set(true, forKey: "SKStoreReviewController_manuallyLaunchOnce")
                showSKStoreReviewControllerIfAvailable_OtherwiseOpenAppStore()
            } else {
                openAppStore()
            }
        } else {
            showSKStoreReviewControllerIfAvailable_OtherwiseOpenAppStore()
        }
        
        #if DEBUG
        print("Debug mode. We will keep app unrated for easier testing.")
        #else
        UserDefaults.standard.set(true, forKey: "user_pressed_rate_5_stars")
        #endif
    }
    
    @IBAction func noThanksButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
