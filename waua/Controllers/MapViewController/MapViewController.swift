//
//  MapViewController.swift
//  waua
//
//  Created by Edin on 9/27/17.
//  Copyright © 2017 qsdbih. All rights reserved.
//

import UIKit
import MapKit
import CoreData

/**
 * Number of requests have to be made before app suggests
 * user to become a PRO user
 */
let kUSE_COUNT_BEFORE_SUGGEST_PRO_ACCOUNT =  7

class MapViewController: UIViewController, NSFetchedResultsControllerDelegate, CLLocationManagerDelegate {
    
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var mapBarButton: UIBarButtonItem!
    @IBOutlet var loadingView: UIView!
    @IBOutlet var connectingLabel: UILabel!
    @IBOutlet var becomeProButton: UIButton!
    @IBOutlet var directionsButton: UIButton!
    
    var centerLocation: Message!
    var isFromHistoryVC = false
    
    // set the text for "Sattelite/Map" view button
    private var _satelliteTitle = ""
    var satelliteTitle: String {
        get {
            return _satelliteTitle
        }
        set(satelliteTitle) {
            _satelliteTitle = satelliteTitle
            mapBarButton.title = NSLocalizedString(satelliteTitle, comment: "")
        }
    }
    
    // get the managed object context
    private var _managedObjectContext: NSManagedObjectContext?
    var managedObjectContext: NSManagedObjectContext? {
        if _managedObjectContext == nil {
            _managedObjectContext = CoreDataManager.shared.context
        }
        return _managedObjectContext
    }
    
    // results fetcher
    private var _fetchedResultsController: [Message]?
    var fetchedResultsController: [Message]? {
        if _fetchedResultsController == nil {
            var messages: [Message]?
            guard let context = managedObjectContext else { return nil }
            let fetchRequest: NSFetchRequest<Message> = Message.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "status == %@", "Accepted")
            let sortDescriptor = NSSortDescriptor(key: "requestDate", ascending: false)
            fetchRequest.sortDescriptors = [sortDescriptor]
        
            do {
                messages = try context.fetch(fetchRequest)
            } catch {
                fatalError("Failed to fetch message: \(error)")
            }
        
            return messages
        }
        else {
            return _fetchedResultsController
        }
    }
    
    private var _locationManager: CLLocationManager?
    func locationManager() -> CLLocationManager {
        if _locationManager == nil {
            _locationManager = CLLocationManager()
            _locationManager?.delegate = self
        }
        return _locationManager!
    }
    
    // view will appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if mapView.mapType == .satellite {
            satelliteTitle = "Map View"
        }
        else {
            satelliteTitle = "Satellite View"
        }
        
        mapView.removeAnnotations(mapView.annotations)
        mapView.removeOverlays(mapView.overlays)
        
        if isFromHistoryVC {
            navigationController?.setNavigationBarHidden(false, animated: animated)
            navigationController?.navigationBar.tintColor = .wauaGreen()
            
            let defaults = UserDefaults.standard
            let count = defaults.integer(forKey: "use-count")
            if count > 0 && ((count & kUSE_COUNT_BEFORE_SUGGEST_PRO_ACCOUNT) == 0) {
                becomePro(nil)
            }
            guard let name = centerLocation.name else { return }
            directionsButton.isHidden = false
            directionsButton.setTitle(String(format: NSLocalizedString("Directions_Button", comment: ""), name), for: UIControlState())
            var size = directionsButton.sizeThatFits(CGSize(width: 300, height: CGFloat.greatestFiniteMagnitude))
            var frame: CGRect = directionsButton.frame
            size.width += 20
            frame.size = size
            frame.origin.x = (view.frame.size.width / 2) - (frame.size.width / 2)
            directionsButton.frame = frame
            
            addLocation(centerLocation)
            let annotation = MKPointAnnotation()
            let location = CLLocationCoordinate2DMake(centerLocation.latitude, centerLocation.longitude)
            annotation.coordinate = location
            guard let title = centerLocation.title, let subtitle = centerLocation.subtitle else { return }
            annotation.title = title
            annotation.subtitle = subtitle
            mapView.selectedAnnotations = [annotation]
            setMapLocation(location)
            let circle = MKCircle(center: location, radius: centerLocation.accuracy)
            mapView.add(circle)
        }
        else {
            directionsButton.isHidden = true
            guard let messages = fetchedResultsController else { return }
            for message in messages {
                addLocation(message)
            }
        }
        locationManager().requestWhenInUseAuthorization()
        //mapView.showsUserLocation = true
        if !AppStoreService.isProUser() {
            if isFromHistoryVC {
                let defaults = UserDefaults.standard
                if !defaults.bool(forKey: "suggest-satellite") {
                    defaults.set(true, forKey: "suggest-satellite")
                    defaults.synchronize()
                    toggleSatelliteView(nil)
                }
            }
        }
    }
    
    // view did load
    override func viewDidLoad() {
        super.viewDidLoad()
        connectingLabel.text = NSLocalizedString("Accessing App Store", comment: "")
        if AppStoreService.isProUser() {
            becomeProButton.removeFromSuperview()
            becomeProButton = nil
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard isFromHistoryVC else { return }
        
        if CLLocationManager.authorizationStatus() == .notDetermined {
            //Do nothing
        }
        else {
            let notFirstRun = UserDefaults.standard.object(forKey: "notFirstRun") as? Bool ?? false
            if notFirstRun {
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5), execute: {
                    self.presentRateAppScreenIfNeeded()
                })
            }
            else {
                UserDefaults.standard.set(true, forKey: "notFirstRun")
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5), execute: {
                    self.presentRateAppScreen()
                })
            }
        }
    }
    
    // gone
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
            navigationController?.setNavigationBarHidden(isFromHistoryVC, animated: animated)
    }
    
    // satellite view
    @IBAction func toggleSatelliteView(_ sender: Any?) {
        if mapView.mapType == .satellite {
            mapView.mapType = .standard
            satelliteTitle = "Satellite View"
        }
        else {
            mapView.mapType = .satellite
            satelliteTitle = "Map View"
        }
    }
    
    @IBAction func becomePro(_ sender: Any?) {
        if AppStoreService.isProUser() {
            //do nothing
        }
        else {
            self.pushIAPVC()
        }
        let defaults = UserDefaults.standard
        defaults.set(0, forKey: "use-count")
        defaults.synchronize()
    }
    
    @IBAction func showDirections(_ sender: Any) {
        if AppStoreService.isProUser() {
            // Create an MKMapItem to pass to the Maps app
            let coordinate = CLLocationCoordinate2DMake(self.centerLocation.latitude, self.centerLocation.longitude)
            let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
            let mapItem = MKMapItem(placemark: placemark)
            mapItem.name = self.centerLocation.name
            // Set the directions mode to "Walking"
            // Can use MKLaunchOptionsDirectionsModeDriving instead
            let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeWalking]
            // Get the "Current User Location" MKMapItem
            let currentLocationMapItem = MKMapItem.forCurrentLocation()
            // Pass the current location and destination map items to the Maps app
            // Set the direction mode in the launchOptions dictionary
            MKMapItem.openMaps(with: [currentLocationMapItem, mapItem], launchOptions: launchOptions)
        }
        else {
            self.pushIAPVC()
        }
    }
    
    private func instantiateIAPVC() -> UIViewController? {
        return storyboard?.instantiateViewController(withIdentifier: "IAPVC") as? IAPViewController
        
    }
    
    func pushIAPVC() {
        if let iapVC = instantiateIAPVC() {
            let window = UIWindow(frame: UIScreen.main.bounds)
            window.rootViewController = UIViewController()
            window.windowLevel = UIWindowLevelAlert + 1;
            window.makeKeyAndVisible()
            window.rootViewController?.present(iapVC, animated: true, completion: nil)
        }
    }
    
    private func presentRateAppScreenIfNeeded() {
        
        var acceptedRequestCounter = UserDefaults.standard.object(forKey: "accepted_request_counter") as? Int ?? 0
        acceptedRequestCounter += 1
        UserDefaults.standard.set(acceptedRequestCounter, forKey: "accepted_request_counter")
        //UserDefaults.standard.synchronize()
        
        //print("Chart screen counter: \(chartScreenCounter)")
        
        let alreadyRate5Stars = UserDefaults.standard.bool(forKey: "user_pressed_rate_5_stars")
        guard !alreadyRate5Stars else { return }
        
        let b1 = acceptedRequestCounter == 5
        let b2 = (acceptedRequestCounter - 5) % 15 == 0
        var b3 = false
        #if DEBUG
        b3 = true
        #endif
        let shouldShowRateAppScreen = b1 || b2 || b3
        guard shouldShowRateAppScreen else { return }
        
        self.presentRateAppScreen()
    }
    
    private func presentRateAppScreen() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let rateAppVC = storyboard.instantiateViewController(withIdentifier: "RateAppViewControllerID") as! RateAppViewController
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = UIViewController()
        window.windowLevel = UIWindowLevelAlert + 1;
        window.makeKeyAndVisible()
        window.rootViewController?.present(rateAppVC, animated: true, completion: nil)
    }
}

extension MapViewController: MKMapViewDelegate {

    //------------------------------------------------------------------------------
    // Map stuff
    //------------------------------------------------------------------------------
    
    // center the map to the user location
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        if !isFromHistoryVC {
            guard let coordinate = userLocation.location?.coordinate else { return }
            setMapLocation(coordinate)
        }
        //Flurry.setLatitude(userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude, horizontalAccuracy: userLocation.location?.horizontalAccuracy, verticalAccuracy: userLocation.location?.verticalAccuracy)
    }
    
    // center the map on the location
    func setMapLocation(_ coord: CLLocationCoordinate2D) {
        // the region
        var region = MKCoordinateRegion()
        region.span.latitudeDelta = 0.010
        region.span.longitudeDelta = 0.010
        region.center = coord
        region = mapView.regionThatFits(region)
        // somethign wrong ?
        if region.center.latitude.isNaN || region.center.longitude.isNaN || region.span.latitudeDelta.isNaN || region.span.longitudeDelta.isNaN {
            return
        }
        // set the region
        mapView.setRegion(region, animated: true)
    }
    
    // simply update the location
    func updateCenterLocation(_ centerLocation: Message) {
        removeLocation(self.centerLocation)
        addLocation(centerLocation)
        self.centerLocation = centerLocation
        let annotation = MKPointAnnotation()
        let location = CLLocationCoordinate2DMake(centerLocation.latitude, centerLocation.longitude)
        annotation.coordinate = location
        guard let title = centerLocation.title, let subtitle = centerLocation.subtitle else { return }
        annotation.title = title
        annotation.subtitle = subtitle
        mapView.selectedAnnotations = [annotation]
        setMapLocation(location)
        let circle = MKCircle(center: location, radius: centerLocation.accuracy)
        mapView.add(circle)
    }

    func addLocation(_ message: Message) {
        let annotation = MKPointAnnotation()
        let location = CLLocationCoordinate2DMake(message.latitude, message.longitude)
        annotation.coordinate = location
        guard let title = message.title, let subtitle = message.subtitle else { return }
        annotation.title = title
        annotation.subtitle = subtitle
        mapView.addAnnotation(annotation)
        let circle = MKCircle(center: location, radius: message.accuracy)
        mapView.add(circle)
        
    }
    
    func updateLocation(_ message: Message) {
        removeLocation(message)
        addLocation(message)
    }

    func removeLocation(_ message: Message) {
        let annotation = MKPointAnnotation()
        let location = CLLocationCoordinate2DMake(message.latitude, message.longitude)
        annotation.coordinate = location
        guard let title = message.title, let subtitle = message.subtitle else { return }
        annotation.title = title
        annotation.subtitle = subtitle
        mapView.removeAnnotation(annotation)
        let circle = MKCircle(center: location, radius: message.accuracy)
        mapView.remove(circle)
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKCircleRenderer(overlay: overlay)
        renderer.fillColor = .blue
        renderer.alpha = 0.15

        return renderer
    }
    
    //------------------------------------------------------------------------------
    // iAd
    //------------------------------------------------------------------------------
    
    func layout(animated: Bool) {
        if AppStoreService.isProUser() {
            return
        }
        let contentFrame: CGRect = view.bounds
        UIView.animate(withDuration: animated ? 0.25 : 0.0, animations: {() -> Void in
            self.mapView.frame = contentFrame
            self.mapView.layoutIfNeeded()
        })
    }
    
    // will rotate
    override func willAnimateRotation(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        satelliteTitle = _satelliteTitle
        if AppStoreService.isProUser() {
            return
        }
        layout(animated: duration > 0.0)
    }
}

