//
//  LocationManager.swift
//  waua
//
//  Created by Edin on 10/11/17.
//  Copyright © 2017 qsdbih. All rights reserved.
//
import UIKit
import Foundation
import CoreLocation

//typealias WatchKitReplyBlock = (_: [AnyHashable: Any]) -> Void

class LocationManager: CLLocationManager, CLLocationManagerDelegate {
    
    static let shared = LocationManager()
    
    var manager: CLLocationManager?
    var coordinates = [Any]()
    //var replyBlock = WatchKitReplyBlock()

    private func setupLocationManager() {
        if manager == nil {
            manager = CLLocationManager()
            manager?.delegate = self
            manager?.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            manager?.distanceFilter = 100.0
        }
        guard let manager = manager else { return }
        if manager.responds(to: #selector(self.requestAlwaysAuthorization)) && .notDetermined == CLLocationManager.authorizationStatus() {
            // we're on iOS+ and haven't been given permission yet
            manager.requestAlwaysAuthorization()
            return
        }
        manager.startUpdatingLocation()
    }

    private func calculateDistancesRelative(to location: CLLocation) {
//        if !replyBlock {
//            return
//        }
//        var distances = [AnyHashable]()
//        (coordinates as NSArray).enumerateObjects({(_ coords: [AnyHashable: Any], _ idx: Int, _ stop: Bool) -> Void in
//            let here = CLLocation(latitude: coords["latitude"], longitude: coords["longitude"])
//            let distance: CLLocationDistance = location.distance(from: here)
//            distances.append(distance)
//        })
//        replyBlock(["distances": distances])
//        replyBlock = nil
//        coordinates = nil
    }

    func calculateDistance(toCoordinates coordinates: [Any], completion block: @escaping (_: [AnyHashable: Any]?) -> Void) {
        manager = CLLocationManager()
        manager?.requestWhenInUseAuthorization()
        switch LocationManager.authorizationStatus() {
        case .denied, .restricted:
            block(nil)
            return
        default:
            break
        }
        self.coordinates = coordinates
        //replyBlock = block.copy()
        setupLocationManager()
    }

    // MARK: - CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways {
            // we got the permission we wanted
            self.manager?.startUpdatingLocation()
        }
        else {
            // we didn't get the permission we wanted
//            if replyBlock {
//                replyBlock(nil)
//                replyBlock = nil
//            }
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("location manager error: \(error)")
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.manager?.stopUpdatingLocation()
        guard let lastLocation = locations.last else { return }
        calculateDistancesRelative(to: lastLocation)
    }
}
