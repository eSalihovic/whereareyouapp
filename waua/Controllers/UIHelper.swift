//
//  UIHelper.swift
//  waua
//
//  Created by Nikola Jovic on 12/01/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//


import UIKit
import MBProgressHUD


class UIHelper {
    
    struct ScreenSize {
        static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    struct DeviceType {
        static let IS_IPHONE_4_OR_LESS =  UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        static let IS_IPHONE_5_OR_LESS = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH <= 568.0
        static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPHONE_X = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
        static let IS_GREATER_THAN_IPHONE_6 = UIDevice.current.userInterfaceIdiom == .phone
            && ScreenSize.SCREEN_MAX_LENGTH > 667.0
            && ScreenSize.SCREEN_MAX_LENGTH <= 736.0
    }

    
    // MBProgressHUD helpers
    
    static func showHUD(_ viewController: UIViewController) {
        let view = hudViewForViewController(viewController)
        showHUD(view: view)
    }
    
    static func showHUD(_ viewController: UIViewController, message: String) {
        let view = hudViewForViewController(viewController)
        showHUD(view: view, message: message, animated: true)
    }
    
    private static func hudViewForViewController(_ viewController: UIViewController) -> UIView {
        return viewController.tabBarController?.view
            ?? viewController.navigationController?.view
            ?? viewController.view
    }
    
    static func showHUD(view: UIView, message: String? = nil, animated: Bool = true) {
        DispatchQueue.main.async {
            let hud = MBProgressHUD.showAdded(to: view, animated: animated)
            hud.label.text = message
            hud.contentColor = .black
        }
    }
    
    static func hideHUD(_ viewController: UIViewController) {
        let view = hudViewForViewController(viewController)
        hideHUD(view: view)
    }
    
    static func hideHUD(view: UIView, animated: Bool = true) {
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: view, animated: animated)
        }
    }
}


