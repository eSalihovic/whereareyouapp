//
//  ExpandedViewController.swift
//  iMessageExtension
//
//  Created by Edin on 11/30/17.
//  Copyright © 2017 qsdbih. All rights reserved.
//

import Foundation
import UIKit

class ExpandedViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var nameTextField: UITextField!
    
    weak var messageVCDelegate: MessagesViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameTextField.delegate = self
        nameTextField.tintColor = .wauaGreen()
        nameTextField.becomeFirstResponder()
        nameTextField.clearsOnBeginEditing = true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let name = textField.text, name != "" {
            messageVCDelegate?.composeMessage(name: name)
            messageVCDelegate?.change(presentationStyle: .compact)
        }
        else {
            let alertController = UIAlertController(title: "Error: Name cannot be empty!",
                                                    message: nil,
                                                    preferredStyle: .alert)
            alertController.view.tintColor = .wauaGreen()
            let okAction = UIAlertAction(title: "OK", style: .default) { (alertAction) in
                self.nameTextField.becomeFirstResponder()
            }
            alertController.addAction(okAction)
            present(alertController, animated: true, completion: nil)
        }
        return true
    }
}
