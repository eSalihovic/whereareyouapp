//
//  CompactViewController.swift
//  iMessageExtension
//
//  Created by Edin on 11/30/17.
//  Copyright © 2017 qsdbih. All rights reserved.
//

import Foundation
import UIKit

class CompactViewController: UIViewController {
    
    @IBOutlet var locateButton: UIButton!
    
    weak var messageVCDelegate: MessagesViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // locate somebody button background
        var btnImage = UIImage(named: "button_outline")
        guard let image = btnImage else { return }
        btnImage = image.resizableImage(withCapInsets: UIEdgeInsetsMake(5, 5, 5, 5))
        locateButton.setBackgroundImage(btnImage, for: .normal)
    }
    
    @IBAction func locateButtonTapped(_ sender: Any) {
        messageVCDelegate?.change(presentationStyle: .expanded)
    }
}
